"use strict";
var assetmanager = require("terraform-connect");

module.exports = function(options, imports, register){
  var debug = imports.debug("site-server");
  debug("start");

  var express = imports.express.getModule();
  var siteServer = {};


  // @publicDir root directory for static content
  siteServer.serveStaticFrom = function(publicDir){

    if (publicDir) {
      debug(".useStatic");
      imports.express.useStatic(express.static(publicDir));
    }

  };

  siteServer.mountStatic = function(mountPoint, publicDir){

    if (publicDir) {
      debug(".useApps");
      imports.express.useApps(mountPoint, express.static(publicDir));
    }

  };

  // @rootDir root directory for Harp to serve dynamic content
  siteServer.serveDynamicFrom = function(rootDir){

    if (rootDir) {
      debug(".useStatic");
      imports.express.useStatic(express.static(rootDir));
      imports.express.useStatic(assetmanager.pipeline({root: rootDir}));
    }
  };

  siteServer.mountDynamic = function(mountPoint, rootDir){

    if (rootDir) {
      debug(".useApps");
      imports.express.useApps(mountPoint, express.static(rootDir));
      imports.express.useApps(mountPoint, assetmanager.pipeline({root: rootDir}));
    }
  };

  debug("register nothing");
  register(null, {
    siteServer: siteServer
  });

};
